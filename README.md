# TasMan

A declarative configuration manager for devices running the Tasmota firmware.

## About TasMan

TasMan is a tool to help manage the Tasmota firmware on devices. It is declarative (like Chef&trade;), so you specify the config you want on your device and TasMan will make sure it is configured properly.

## License

TasMan is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

TasMan is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with TasMan. If not, see [http://www.gnu.org/licenses/](http://www.gnu.org/licenses/).

## Relation to Tasmota

This software is not affilliated with the Tasmota project. It is a project by a fan hoping to help other users.
