# frozen_string_literal: true

require_relative "tasman/version"

module Tasman
  class Error < StandardError; end
  # Your code goes here...
end
