# frozen_string_literal: true

require_relative "lib/tasman/version"

Gem::Specification.new do |spec|
  spec.name = "tasman"
  spec.version = Tasman::VERSION
  spec.authors = ["Trey Chandler"]
  spec.email = ["trey@401-studios.com"]

  spec.summary = "Manager utility for devices running Tasmota."
  # spec.description = 'TODO: Write a longer description or delete this line.'
  spec.homepage = "https://gitlab.com/trey_makes_things/tasmanager"
  spec.required_ruby_version = ">= 2.6.0"

  spec.metadata["allowed_push_host"] = "TODO: Set to your gem server 'https://example.com'"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/trey_makes_things/tasmanager"
  spec.metadata["changelog_uri"] = "https://gitlab.com/trey_makes_things/tasmanager/CHANGELOG.md"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(__dir__) do
    `git ls-files -z`.split("\x0").reject do |f|
      (f == __FILE__) || f.match(%r{\A(?:(?:bin|test|spec|features)/|\.(?:git|travis|circleci)|appveyor)})
    end
  end
  spec.bindir = "exe"
  spec.executables = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  # Uncomment to register a new dependency of your gem
  spec.add_dependency "thor", "~> 1.2.1"

  # For more information and examples about making a new gem, check out our
  # guide at: https://bundler.io/guides/creating_gem.html

  spec.license = "GPL3"
end
